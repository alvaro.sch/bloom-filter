# override compiler only if it's the default
ifeq ($(CC),cc)
	CC = clang++
endif

CFLAGS = -std=c++20 -O3 -march=native
LDFLAGS =

SOURCE = $(wildcard src/*.cpp)
OBJECT = $(SOURCE:src/%.cpp=%.o)
TARGET = bloom_filter.out

$(TARGET): $(OBJECT)
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $(OBJECT)

%.o: src/%.cpp
	$(CC) $(CFLAGS) -c $< -o $@

run: $(TARGET)
	./$(TARGET)

clean:
	$(RM) *.o $(TARGET)

