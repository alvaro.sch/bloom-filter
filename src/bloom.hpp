#pragma once

#include <cmath>
#include <ctime>
#include <random>
#include <vector>

#include "bitvec.hpp"
#include "hash.hpp"

template <typename T, typename H = StdHasher>
    requires Hasher<H, T>
class BloomFilter {
    struct HashFunction {
        size_t _a, _b, _M;

        size_t operator()(const H &h, const T &a) const {
            return (_a * h.hash(a) + _b) % _M;
        }
    };

    std::vector<HashFunction> _hash_fs;
    BitVector _bits;
    H _hasher;

   public:
    BloomFilter(size_t m, size_t k, H hasher = H()) : _bits(m), _hasher(hasher) {
        _hash_fs.reserve(k);

        std::mt19937 rng(time(nullptr));
        for (auto i = 0; i < k; ++i) {
            _hash_fs.push_back(HashFunction{rng(), rng(), m});
        }
    }

    static BloomFilter from_fp(double fp, size_t max_size, H hasher = H()) {
        size_t m = std::ceil(1.44 * max_size * log2(1 / fp));
        size_t k = log2(1.0 / fp);

        return BloomFilter(m, k, hasher);
    }

    void insert(const T &a) {
        for (auto &hash_f : _hash_fs) {
            _bits.set(hash_f(_hasher, a));
        }
    }

    bool contains(const T &a) const {
        for (auto &hash_f : _hash_fs) {
            if (!_bits.is_set(hash_f(_hasher, a))) {
                return false;
            }
        }
        return true;
    }
};
