#include <cstddef>
#include <iostream>

#include "bloom.hpp"
#include "hash.hpp"

struct TestParams {
    // size of the bitvec
    size_t M;
    // number of elements to insert
    size_t n;
    // number of hashfunctions
    size_t k;
};

// for debugging purposes
template <typename T>
concept Named = requires {
                    { T::type_name() };
                };

template <typename H>
    requires Hasher<H, int>
double simulate_single(size_t M, size_t n, size_t k) {
    BloomFilter<int, H> filter(M, k);

    for (auto i = 0; i < n; ++i) {
        filter.insert(2 * i);
    }

    size_t false_positives = 0;
    // test for odd numbers which aren't in the ste
    for (auto i = 0; i < n; ++i) {
        if (filter.contains(2 * i + 1)) {
            ++false_positives;
        }
    }
    // test for even numbers which aren't in the set
    for (auto i = n; i < 2 * n; ++i) {
        if (filter.contains(2 * i)) {
            ++false_positives;
        }
    }

    return (double)false_positives / (2.0 * n);
}

template <typename H>
    requires Hasher<H, int> && Named<H>
void simulate(size_t count, const std::vector<TestParams> &test_vectors) {
    for (const auto &[M, n, k] : test_vectors) {
        double avg_fp = 0.0;

        std::cout << H::type_name() << ',';
        std::cout << M << ',';
        std::cout << n << ',';
        std::cout << k << ',';

        for (auto i = 0; i < count; ++i) {
            avg_fp += simulate_single<H>(M, n, k);
        }

        std::cout << avg_fp / count << '\n';
    }
}

template <typename...>
struct TestSuite {};

template <typename Head>
struct TestSuite<Head> {
    static void run(size_t simulations, const std::vector<TestParams> &test_vectors) {
        simulate<Head>(simulations, test_vectors);
    }
};

template <typename Head, typename... Tail>
struct TestSuite<Head, Tail...> {
    static void run(size_t simulations, const std::vector<TestParams> &test_vectors) {
        simulate<Head>(simulations, test_vectors);
        TestSuite<Tail...>::run(simulations, test_vectors);
    }
};
