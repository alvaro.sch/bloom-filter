#pragma once

#include <cstddef>
#include <cstdint>
#include <vector>

class BitVector {
    std::vector<uint8_t> _bytes;

   public:
    BitVector() {}
    BitVector(size_t m) : _bytes(((m - 1) >> 3) + 1, 0) {}

    void set(size_t i) {
        size_t effective_id = i >> 3;
        uint8_t set_mask = 1 << (i & 0x7);
        _bytes[effective_id] |= set_mask;
    }

    void clear(size_t i) {
        size_t effective_id = i >> 3;
        uint8_t clear_mask = ~(1 << (i & 0x7));
        _bytes[effective_id] &= clear_mask;
    }

    bool is_set(size_t i) const {
        size_t effective_id = i >> 3;
        uint8_t read_mask = 1 << (i & 0x7);
        return _bytes[effective_id] & read_mask;
    }
};
