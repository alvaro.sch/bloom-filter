#pragma once

#include <bit>
#include <concepts>
#include <cstddef>
#include <cstdint>
#include <cstring>
#include <functional>
#include <string_view>
#include <type_traits>

// H can hash values of type T
template <typename H, typename T>
concept Hasher = requires(H h, const T &a) {
                     { h.hash(a) } -> std::convertible_to<size_t>;
                 };

// T is hashable with the std library
template <typename T>
concept Hash = requires(const T &a) {
                   { std::hash<T>{}(a) } -> std::convertible_to<size_t>;
               };

// default std library hasher
struct StdHasher {
    // unused, placed to make it compatible with the other hashers
    StdHasher() {}
    StdHasher(size_t _seed) {}

    template <Hash H>
    size_t hash(const H &a) const {
        return std::hash<H>{}(a);
    }

    static const char *type_name() {
        return "StdHasher";
    }
};

// the type is primitive and not a pointer
template <typename T>
concept Plain = std::is_arithmetic<T>::value || std::is_enum<T>::value;

struct JenkinsHasher {
    JenkinsHasher() : _seed(0) {}
    JenkinsHasher(uint32_t seed) : _seed(seed) {}

    static const char *type_name() {
        return "JenkinsHasher";
    }

    template <Plain T>
    uint32_t hash(const T &a) const {
        const char *key = static_cast<const char *>(static_cast<const void *>(&a));
        return _hash(key, sizeof(T));
    }

    uint32_t hash(std::string_view s) const {
        return _hash(s.data(), s.length());
    }

   private:
    uint32_t _seed;

    // jenkins 'one at a time' hash
    // https://en.wikipedia.org/wiki/Jenkins_hash_function
    uint32_t _hash(const char *key, size_t len) const {
        size_t i = 0;

        uint32_t hash = _seed;
        while (i != len) {
            hash += key[i++];
            hash += hash << 10;
            hash ^= hash >> 6;
        }

        hash += hash << 3;
        hash ^= hash >> 11;
        hash += hash << 15;

        return hash;
    }
};

struct FxHasher {
    FxHasher() : _seed(0) {}
    FxHasher(size_t seed) : _seed(seed) {}

    static const char *type_name() {
        return "FxHasher";
    }

    template <Plain T>
    size_t hash(const T &a) const {
        const char *key = static_cast<const char *>(static_cast<const void *>(&a));
        return _hash(key, sizeof(T));
    }

    size_t hash(std::string_view s) const {
        return _hash(s.data(), s.length());
    }

   private:
    size_t _seed;

    static const size_t K = 0x517cc1b727220a95;

    static void _add_to_hash(size_t &hash, size_t i) {
        hash = (std::rotl(hash, 5) ^ i) * K;
    }

    template <typename T>
    static size_t _read(const char *key, size_t &len) {
        T i;
        memcpy(&i, key, sizeof(T));

        len -= sizeof(T);

        return i;
    }

    // janky adaptation of https://github.com/rust-lang/rustc-hash
    // may behave differently across systems with different endianness
    size_t _hash(const char *key, size_t len) const {
        size_t hash = _seed;

        static_assert(sizeof(size_t) <= 8);
        while (len >= sizeof(size_t)) {
            _add_to_hash(hash, _read<size_t>(key, len));
            key += sizeof(size_t);
        }
        if ((sizeof(size_t) > 4) && len >= 4) {
            _add_to_hash(hash, _read<uint32_t>(key, len));
            key += sizeof(uint32_t);
        }
        if ((sizeof(size_t) > 2) && len >= 2) {
            _add_to_hash(hash, _read<uint16_t>(key, len));
            key += sizeof(uint16_t);
        }
        if ((sizeof(size_t) > 1) && len >= 1) {
            _add_to_hash(hash, *key);
        }

        return hash;
    }
};
