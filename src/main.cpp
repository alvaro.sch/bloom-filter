#include "hash.hpp"
#include "test_util.hpp"

int main() {
    // big enough prime number
    const size_t M = 10069;
    const size_t SIM_COUNT = 10000;

    std::vector<TestParams> test_vectors;
    for (size_t n = 1000; n < 10001; n += 1000) {
        test_vectors.push_back({M, n, 1});
    }
    TestSuite<StdHasher, JenkinsHasher, FxHasher>::run(SIM_COUNT, test_vectors);

    for (size_t n = 1000; n < 10001; n += 1000) {
        test_vectors.clear();
        for(size_t k = 1; k < 11; ++k) {
            test_vectors.push_back({M, n, k});
        }
        TestSuite<StdHasher, JenkinsHasher, FxHasher>::run(SIM_COUNT, test_vectors);
    }

    return 0;
}
