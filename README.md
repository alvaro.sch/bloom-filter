# Bloom Filter

A simple implementation of [Bloom Filters](https://en.wikipedia.org/wiki/Bloom_filter) in C++20 for an assignment on an Advanced Data Structures course.

Most of the code is implemented in header files because of simplicity and monomorphization.

## Structure

- `bitvec.hpp` - bitvector class.
- `hash.hpp` - hash related concepts and hashers.
- `bloom.hpp` - implementation of bloom filters.
- `test_util.hpp` - utilities for running tests and simulations.
- `main.cpp` - compares the probability for false-positives across all hashers in various cases.

## Compiling and running the code

Even if there's only one source file, there's a makefile.

```sh
make
```

It uses `clang++` by default, but it can be overriden i.e.

```sh
CC=g++ make
```

If not present, the only hard requirement is to enable C++20 (`-std=c++20`).

Running all simulations may take a lot of time (+17 minutes) I didn't really have time to worry for multithreading and getting synchronous output.
Runtime can be decreased by doing less simulations per case, omiting certain test cases or removing other hashers per test.

